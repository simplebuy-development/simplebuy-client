angular.module('simplebuy', [
  'ui.router',
  'simplebuy.templates',
  'simplebuy.Home'
]);

angular.module('simplebuy.Home', []);

angular.module('simplebuy')
  .config(function ($httpProvider, $stateProvider, $urlRouterProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');

    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('home', {
        url: '/',
        //templateProvider: template('home/controllers/home.html')
        templateUrl: 'home/controllers/home.html'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'home/controllers/login.html'
      })
  });

angular.module('simplebuy').factory('AuthInterceptor', function ($rootScope, $q, $window) {
    return {
      request: function (config) {
        config.headers = config.headers || {};
        if ($window.sessionStorage.token) {
          config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
        }
        return config;
      },
      response: function (response) {
        if (response.status === 401) {
          // handle the case where the user is not authenticated
        }
        return response || $q.when(response);
      }
    };
});