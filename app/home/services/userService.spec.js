/**
 * Created by allebdev on 13.10.14.
 */
describe('simplebuy.Home', function () {

  beforeEach(module('simplebuy.Home'));

  describe('UserService', function () {
    var user = {username: 'username', password: 'password'};
    var invalidUser = {username: 'invaliduser', password: 'invalidpassword'};
    var token = {token: 'TOKENDATA'};

    var UserService, $httpBackend, $window;
    beforeEach(inject(function (_$window_, _$httpBackend_, _UserService_) {
      UserService = _UserService_;
      $window = _$window_;
      $httpBackend = _$httpBackend_;
      $httpBackend.when('POST', '/login', user).respond(token);
      $httpBackend.when('POST', '/login', invalidUser).respond(401);
      $httpBackend.when('POST', '/register', user).respond(token);
      $httpBackend.when('POST', '/register', invalidUser).respond(401);
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
      delete $window.sessionStorage.token;
    });

    it('should can login', function () {
      expect(UserService.login).toBeDefined();
    });

    it('should save token to sessionStorage after success auth', function () {
      $httpBackend.expect('POST', '/auth', user).respond(token);
      expect($window.sessionStorage.token).toBeUndefined();
      UserService.login(user);
      $httpBackend.flush();
      expect($window.sessionStorage.token).toBe(token.token);
    });

    it('should not save any token if auth failed', function () {
      $httpBackend.expect('POST', '/auth', invalidUser).respond(401);
      UserService.login(invalidUser);
      $httpBackend.flush();
      expect($window.sessionStorage.token).toBeUndefined();
    });

    it('should can logout', function () {
      expect(UserService.logout).toBeDefined();
    });

    it('should delete token from sessionStorage when logout', function () {
      $window.sessionStorage.token = {};
      expect($window.sessionStorage.token).toBeDefined();
      UserService.logout();
      expect($window.sessionStorage.token).toBeUndefined();
    });

    it('should can register a user', function () {
      expect(UserService.register).toBeDefined();
    });

    it('should save a token if register is success', function () {
      expect($window.sessionStorage.token).toBeUndefined();
      $httpBackend.expect('POST', '/register', user);
      UserService.register(user);
      $httpBackend.flush();
      expect($window.sessionStorage.token).toBe(token.token);
    });

    it('should not save any token if register has been failed', function () {
      expect($window.sessionStorage.token).toBeUndefined();
      $httpBackend.expect('POST', '/register', invalidUser);
      UserService.register(invalidUser);
      $httpBackend.flush();
      expect($window.sessionStorage.token).toBeUndefined();
    });
  });

});