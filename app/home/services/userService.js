/**
 * Created by allebdev on 05.10.14.
 */
angular.module('simplebuy.Home').factory('UserService', function ($http, $window, $q /*, NotifyService */) {
  return {
    login: function (user) {
      var deferred = $q.defer();
      $http.post('/auth', user)
        .success(function (data, status, headers, config) {
          $window.sessionStorage.token = data.token;
          deferred.resolve();
        })
        .error(function (data, status, headers, config) {
          delete $window.sessionStorage.token;
          deferred.reject(data);
        });
      return deferred.promise;
    },
    logout: function () {
      delete $window.sessionStorage.token;
    },
    register: function (user) {
      var deferred = $q.defer();
      $http.post('/register', user)
        .success(function (data, status, headers, config) {
          $window.sessionStorage.token = data.token;
          deferred.resolve();
        })
        .error(function (data, status, headers, config) {
          delete $window.sessionStorage.token;
          deferred.reject();
        });
      return deferred;
    }
  };
});
