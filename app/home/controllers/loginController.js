/**
 * Created by allebdev on 21.09.14.
 */
angular.module('simplebuy.Home').controller('LoginCtrl', function ($scope, $state, UserService) {
  $scope.login = function () {
    if(!$scope.loginForm.$valid) return;
    $scope.errorMessage = '';
    UserService.login($scope.user)
      .then(function () {
        $state.go('profile');
      }, function (error) {
        $scope.errorMessage = error;
      });
  };
});