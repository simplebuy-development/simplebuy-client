/**
 * Created by allebdev on 14.10.14.
 */
describe('simplebuy.Home', function () {
  beforeEach(module('simplebuy.Home'));

  describe('LoginCtrl', function () {
    var user, LoginCtrl, $scope, $rootScope, UserServiceMock, $stateMock, $q;

    function createController($controller) {
      return $controller('LoginCtrl', {
        $scope: $scope,
        $state: $stateMock,
        UserService: UserServiceMock
      });
    }

    function setLoginFormIsValid() {
      $scope.loginForm.$valid = true;
    }

    beforeEach(inject(function ($controller, _$rootScope_, _$q_) {
        user = {username: 'username', password: 'password'};
        $q = _$q_;
        $rootScope = _$rootScope_;
        $stateMock = {go: angular.noop};
        $scope = $rootScope.$new();
        $scope.user = user;
        $scope.loginForm = {$valid: false};
        UserServiceMock = {
          login: function () {
            return $q.defer().promise
          }
        };
        LoginCtrl = createController($controller);
      })
    );

    it('should be present and have login function', function () {
      expect(LoginCtrl).toBeDefined();
      expect($scope.login).toBeDefined();
    });

    it('should use UserService to login if form is valid', function () {
      spyOn(UserServiceMock, 'login').and.returnValue($q.when());
      $scope.loginForm.$valid = true;
      $scope.login();
      expect(UserServiceMock.login).toHaveBeenCalledWith(user);
    });

    it('should switch state to "profile" if login is ok', function () {
      spyOn(UserServiceMock, 'login').and.returnValue($q.when());
      spyOn($stateMock, 'go');
      setLoginFormIsValid();
      $scope.login();
      $rootScope.$apply();
      expect(UserServiceMock.login).toHaveBeenCalled();
      expect($stateMock.go).toHaveBeenCalledWith('profile');
    });

    it('should not switch state if login has been failed', function () {
      spyOn(UserServiceMock, 'login').and.returnValue($q.reject());
      spyOn($stateMock, 'go');
      setLoginFormIsValid();
      $scope.login();
      $rootScope.$apply();
      expect(UserServiceMock.login).toHaveBeenCalled();
      expect($stateMock.go).not.toHaveBeenCalled();
    });

    it('should set error message if login has been failed', function () {
      var error = 'error';
      spyOn(UserServiceMock, 'login').and.returnValue($q.reject(error));
      $scope.loginForm.$valid = true;
      $scope.login();
      $rootScope.$apply();
      expect(UserServiceMock.login).toHaveBeenCalled();
      expect($scope.errorMessage).toBe(error);
    });

    it('should do nothing if form is invalid', function () {
      spyOn(UserServiceMock, 'login');
      $scope.loginForm.$valid = false;
      $scope.login();
      expect(UserServiceMock.login).not.toHaveBeenCalled();
    });
  });
});