"use strict";

var gulp = require('gulp'),
  gulpIf = require('gulp-if'),
  rimraf = require('gulp-rimraf'),
  filter = require('gulp-filter'),
  concat = require('gulp-concat'),
  sourcemaps = require('gulp-sourcemaps'),
  ngAnnotate = require('gulp-ng-annotate'),
  uglify = require('gulp-uglify'),
  bowerFiles = require('main-bower-files'),
  templateCache = require('gulp-angular-templatecache'),
  minifyCss = require('gulp-minify-css'),
  autoprefixer = require('gulp-autoprefixer'),
  gutil = require('gulp-util'),
  fs = require('fs'),
  livereload = require('gulp-livereload'),
  express = require('express');

var appName = 'simplebuy',
  serverPort = 8080,
  debugVendor = false;

var rootPaths = {
  src: 'app',
  dest: 'dist'
};

var paths = {
  src: {
    root: rootPaths.src,
    app: rootPaths.src + '/' + appName.toLowerCase() + '.js',
    js: rootPaths.src + '/**/!(*.spec).js',
    css: rootPaths.src + '/**/*.css',
    templates: rootPaths.src + '/**/!(index).html',
    index: rootPaths.src + '/index.html',
    api: 'api'
  },
  dest: {
    root: rootPaths.dest,
    vendor: rootPaths.dest + '/vendor',
    index: rootPaths.dest + '/index.html',
    api: 'api'
  }
};

function startServer() {
  var server = require('./server.js')();
  fs.mkdir(paths.dest.root);
  server.use(express.static(fs.realpathSync(paths.dest.root)));
  //server.use('/api', express.static(fs.realpathSync(paths.dest.api), {extensions: 'json'}));
  server.use('/api', server.resources(paths.dest.api));
  server.listen(serverPort);
}

gulp.task('clean', function () {
  return gulp.src(paths.dest.root, {read: false}).
    pipe(rimraf());
});

function copyIndex() {
  gutil.log('copy index file...');
  return gulp.src('app/index.html')
    .pipe(gulp.dest(paths.dest.root))
    .on('end', notifyLivereload('all'));
}

function bundleVendor(cb) {
  gutil.log('bundle vendor libraries...');
  var jsFilter = filter('*.js');
  var cssFilter = filter('*.css');
// Build vendor js
  gulp.src(bowerFiles())
    .pipe(jsFilter)
    .pipe(concat('vendor.js'))
    .pipe(gulpIf(debugVendor, uglify()))
    .pipe(gulp.dest(paths.dest.vendor));
// Build vendor css
  jsFilter.restore({end: true})
    .pipe(cssFilter)
    .pipe(concat('vendor.css'))
    .pipe(minifyCss({keepSpecialComments: (debugVendor ? '*' : 0)}))
    .pipe(gulp.dest(paths.dest.vendor));
// Copy fonts
  cssFilter.restore({end: true})
    .pipe(filter(function (file) {
      return /font/.test(file.path);
    }))
    .pipe(gulp.dest(paths.dest.vendor + '/fonts'));
// Done
  cb && cb();
  notifyLivereload('all');
}

function bundleTemplates() {
  gutil.log('bundle templates...');
  return gulp.src([paths.src.root + '/**/*.html', '!' + paths.src.root + '/index.html'])
    //.pipe(filter())
    .pipe(templateCache({module: 'simplebuy.templates', standalone: true}))
    .pipe(gulp.dest(paths.dest.root))
    .on('end', notifyLivereload('templates.js'));
}

function bundleStyles() {
  gutil.log('bundle styles...');
  return gulp.src(paths.src.css)
    .pipe(autoprefixer('last 2 versions'))
    .pipe(concat(appName + '.min.css'))
    .pipe(minifyCss())
    .pipe(gulp.dest(paths.dest.root))
    .on('end', notifyLivereload(appName + '.min.css'));
}

function bundleApp() {
  gutil.log('bundle angular app...');
  return gulp.src([paths.src.app, paths.src.js])
    .pipe(sourcemaps.init())
    .pipe(ngAnnotate({add: true}))
    .pipe(concat(appName + '.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.dest.root))
    .on('end', notifyLivereload(appName + '.min.js'));
}

gulp.task('vendor', function () {
  bundleVendor();
});

gulp.task('styles', function () {
  return bundleStyles();
});

gulp.task('templates', function () {
  return bundleTemplates();
});

gulp.task('js', function () {
  return bundleApp();
});

gulp.task('build', ['clean'], function () {
  copyIndex();
  bundleVendor();
  bundleStyles();
  bundleTemplates();
  bundleApp();
});

gulp.task('watch', ['build'], function () {
  gulp.watch(paths.src.js, run(bundleApp));
  gulp.watch(paths.src.css, run(bundleStyles));
  gulp.watch(paths.src.templates, run(bundleTemplates));
  gulp.watch(paths.src.index, run(copyIndex));
  startServer();
  livereload.listen();
});

gulp.task('default', ['watch']);

function run(fn) {
  return function (event) {
    gutil.log('File ' + event.path + ' was ' + event.type + ', starting ' + fn.name);
    fn();
  }
}

function notifyLivereload(filename) {
  return function () {
    var server = livereload({auto: false});
    if (filename === 'all') {
      filename = paths.dest.index
    }
    server.changed(filename)
  }
}