/**
 * Created by allebdev on 05.10.14.
 */

var
  express = require('express'),
  expressJwt = require('express-jwt'),
  jwt = require('jsonwebtoken'),
  bodyParser = require('body-parser'),
  connectLr = require('connect-livereload'),
  _ = require('underscore'),
  fs = require('fs'),
  secret = 'simplebuy'
  ;

var customer = {
  id: 100,
  email: 'customer@simplebuy.org',
  username: 'customer',
  password: 'customer',
  type: 'customer'
};

var seller = {
  id: 200,
  email: 'seller@simplebuy.org',
  username: 'seller',
  password: 'seller',
  type: 'seller'
};

const ACCESS_FILE = '/.access.json';
const DEFAULT_EXT = 'json';

function getUser(username, password) {
  var user = _.filter([customer, seller], function (user) {
    return user.username === username && user.password == password;
  });
  return user.length ? {id: user[0].id, type: user[0].type} : null;
}

function buildPath() {
  var parts = Array.prototype.slice.call(arguments || [], 0);
  parts = parts.concat('.', DEFAULT_EXT);
  return parts.join('');
}

module.exports = function () {
  var server = express();
  server.use(connectLr());
  server.use('/api', expressJwt({secret: secret}));
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({extended: false}));

  server.post('/auth', function (req, res) {
    var user = getUser(req.body.username, req.body.password);
    if (!user) {
      res.status(401).send('Incorrect username or password');
      return;
    }
    var token = jwt.sign(user, secret);
    res.json({token: token});
  });

  server.checkAccess = function (path, resource, method, user) {
    var accessFile = path + ACCESS_FILE;
    if (!fs.existsSync(accessFile)) {
      return true;
    }
    var grants = JSON.parse(fs.readFileSync(accessFile));
    if (grants == null || grants[resource] == null) return true;
    var grant = grants[resource][method] ? grants[resource][method] : grants[resource];
    var role = grant.role;
    if (!role || role === 'any' || role === '*') return true;
    if (!user) return false;
    return Array.isArray(role) ? role.indexOf(user.type) >= 0 : role === user.type;
  };

  server.resources = function (basePath) {
    return function (req, res /*, next */) {
      var lastSeparator = req.path.lastIndexOf('/');
      var path = req.path.substring(1, lastSeparator);
      var resource = req.path.substring(lastSeparator + 1);
      var method = req.method.toLowerCase();

      var fn = buildPath(basePath, path, resource, '.', method);
      if (!fs.existsSync(fn)) fn = buildPath(basePath, path, resource);
      if (!fs.existsSync(fn)) res.send(404).end();
      if (!server.checkAccess(basePath + path, resource, method, req.user)) res.send(403).end();
      res.sendFile(fs.realpathSync(fn));
    }
  };

  return server;
};

