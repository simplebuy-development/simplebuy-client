#!/bin/bash

REPO_URL='https://bitbucket.org/simplebuy-development/simplebuy-client.git'
PROJECT_DIR='simplebuy-client'

NPM=`which npm`
GIT=`which git`
NIX_APTGET=`which apt-get`
NIX_YUM=`which yum`
MAC_BREW=`which brew`
MAC_FINK=`which fink`
MAC_PORT=`which port`
BOWER=`which bower`
KARMA=`which karma`
PHANTOMJS=`which phantomjs`

function fail {
    echo $1
    exit 1
}

function install_nodejs {
    echo "Installing NodeJS..."
    if [ "${NIX_APTGET}" ]; then
        sudo apt-get install curl
        curl -sL https://deb.nodesource.com/setup | sudo bash -
        sudo apt-get install nodejs
    fi
    [ "${NIX_YUM}" ] && sudo yum install nodejs npm --enablerepo=epel
    [ "${MAC_BREW}" ] && sudo brew install node
    [ "${MAC_FINK}" ] && sudo fink install node
    [ "${MAC_PORT}" ] && sudo port install node
    NPM=`which npm`
    if [ -z ${NPM} ]; then
        echo 'Cannot install NodeJS!\nPlease install it manually from: http://nodejs.org/\n'
        exit 1
    fi
}

function install_git {
    echo "Installing GIT..."
    [ "${NIX_APTGET}" ] && sudo apt-get -y install git
    [ "${NIX_YUM}" ] && sudo yum -y install git
    [ "${MAC_PORT}" ] && sudo port install git-core +svn +doc +bash_completion +gitweb
    GIT=`which git`
    if [ -z ${GIT} ]; then
        echo 'Cannot install GIT!\nPlease install it manually from: http://git-scm.com/'
        exit 1
    fi
}

function install_global_utils {
    echo "Check global utils..."
    if [ -n "${NPM}" ]; then
        echo "NodeJS was found in ${NPM}"
    else
        install_nodejs
        [ -n "${NPM}" ] || fail "Can't install NodeJS!"
    fi
    if [ -n "${BOWER}" ]; then
        echo "Bower was found in ${BOWER}"
    else
        echo "Installation Bower..."
        sudo npm install -g bower
        [ -n "${BOWER}" ] || fail "Can't install Bower!"
    fi
    if [ -n "${KARMA}" ]; then
        echo "Karma CLI was found in ${KARMA}"
    else
        echo "Installation Karma CLI..."
        sudo npm install -g karma-cli
        [ -n "${KARMA}" ] || fail "Can't install Karma CLI!"
    fi
    if [ -n "${PHANTOMJS}" ]; then
        echo "PhantomJS was found in ${PHANTOMJS}"
    else
        sudo npm install -g phantomjs
        [ -n "${PHANTOMJS}" ] || "Can't install PhantomJS!"
    fi
}

echo '============================================'
echo '| SimpleBuy Web Client installation script |'
echo '============================================'

[ -n "${GIT}" ] || install_git

if [ ! -e package.json ]; then
    if [ -e ../package.json ]; then
        cd ..
    elif [ -e "$PROJECT_DIR/package.json" ]; then
        cd $PROJECT_DIR
    else
        echo 'There is no found the project root directory!'
        read -p "Do you want to clone it from ${REPO_URL} [y/n]?" -n 1 SHOULD_CLONE
        echo
        if [[ ! $SHOULD_CLONE =~ ^[Yy]$ ]]; then
            fail 'Installation has been interrupted by user!'
        fi
#        read -p "Clone project into current directory [y/n]?" -n 1 IN_CURRENT_DIRECTORY
#        echo
#        [[ $IN_CURRENT_DIRECTORY =~ ^[Yy]$ ]] && PROJECT_DIR='.'
        git clone $REPO_URL $PROJECT_DIR
        cd $PROJECT_DIR
    fi
fi

[ -e package.json ] || fail "You must run script from the project's root directory"

install_global_utils

echo "Installation NodeJS dependencies..."
sudo npm install || fail "Can't install NodeJS dependencies!"
echo "Installation Bower dependencies..."
bower install || fail "Can't install Bower dependencies!"
echo 'Instalation is SUCCESSFUL!'