# SimpleBuy

This project is an application skeleton for [SimpleBuy](http://simplebuy.org/) web app.

## Getting Started

To setup environment under Mac or *nix systems and install all necessary software
you can just run ./scripts/install.sh from a root directory of the project.

### Prerequisites

You need git to work with remote repository. You can get git from
[http://git-scm.com/](http://git-scm.com/).

We also use a number of node.js tools to initialize and test app. You must have node.js and
its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).

Note: you can install all prerequisites just run script as write above

### Clone SimpleBuy

Clone the angular-seed repository using [git][git]:

```
git clone https://bitbucket.org/simplebuy-development/simplebuy-client.git
cd simplebuy-client
```

### Create Environment

Just start `script/install.sh` script and wait while it installs all components is needed

### Install Dependencies

We have two kinds of dependencies in this project: tools and angular framework code.  The tools help
us manage and test the application.

* We get the tools we depend upon via `npm`, the [node package manager][npm].
* We get the 3rd party libraries via `bower`, a [client-side code package manager][bower].

We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
npm install
```

Behind the scenes this will also call `bower install`.  You should find that you have two new
folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `bower_components` - contains the angular framework files and other client-side libraries

*Note that the `bower_components` folder would normally be installed in the root folder but
here this location changed through the `.bowerrc` file.  Putting it in the app folder makes
it easier to serve the files by a webserver.*

### Run the Application

We have preconfigured the project with a simple development web server.  The simplest way to start
this server is:

```
npm start
```

Now browse to the app at `http://localhost:8080/`.


## Directory Layout

```
bower_components/     --> client-side 3rd-party libraries which Bower manages
dist/                 --> production app version stored here
api/                  --> json stub files for REST API calls (for HTTP GET requests only)
app/                  --> all of the source files for the application
  {module}/             --> units of specific module
    controllers/          --> controllers of specific module
    services/             --> services of specific module
    directives/           --> directives of specific module
  simplebuy.js          --> main application module
  simplebuy.css         --> default stylesheet
  index.html            --> app layout file (the main html template file of the app)
karma.conf.js         --> config file for running unit tests with Karma
e2e-tests/            --> end-to-end tests
  protractor-conf.js    --> Protractor config file
  scenarios.js          --> end-to-end scenarios to be run by Protractor
```

## Testing

There are two kinds of tests in the application: Unit tests and End to End tests.
### Running Unit Tests

The angular-seed app comes preconfigured with unit tests. These are written in
[Jasmine][jasmine], which we run with the [Karma Test Runner][karma]. We provide a Karma
configuration file to run them.

* the configuration is found at `karma.conf.js`
* the unit tests are found near the code they are testing and are named as `*.spec.js`.
```
For example:
  app/home/controllers/loginController.js       - unit definition
  app/home/controllers/loginController.spec.js  - unit test
```

The easiest way to run the unit tests is to use the supplied npm script:

```
npm test
```

This script will start the Karma test runner to execute the unit tests. Moreover, Karma will sit and
watch the source and test files for changes and then re-run the tests whenever any of them change.
This is the recommended strategy; if your unit tests are being run every time you save a file then
you receive instant feedback on any changes that break the expected code functionality.

You can also ask Karma to do a single run of the tests and then exit.  This is useful if you want to
check that a particular version of the code is operating as expected.  The project contains a
predefined script to do this:

```
npm run test-single-run
```

Also if we use WebStorm by JetBrains you can setup Karma Run Configuration integrated in it.

### End to end testing

The angular-seed app comes with end-to-end tests, again written in [Jasmine][jasmine]. These tests
are run with the [Protractor][protractor] End-to-End test runner.  It uses native events and has
special features for Angular applications.

* the configuration is found at `e2e-tests/protractor-conf.js`
* the end-to-end tests are found in `e2e-tests/scenarios.js`

Protractor simulates interaction with our web app and verifies that the application responds
correctly. Therefore, our web server needs to be serving up the application, so that Protractor
can interact with it.

```
npm start
```

In addition, since Protractor is built upon WebDriver we need to install this.  The angular-seed
project comes with a predefined script to do this:

```
npm run update-webdriver
```

This will download and install the latest version of the stand-alone WebDriver tool.

Once you have ensured that the development web server hosting our application is up and running
and WebDriver is updated, you can run the end-to-end tests using the supplied npm script:

```
npm run protractor
```

This script will execute the end-to-end tests against the application being hosted on the
development server.


## Updating 

Framework libraries code and tools are acquired through package managers (npm and
bower) you can use these tools instead to update the dependencies.

You can update the tool dependencies by running:

```
npm update
```

This will find the latest versions that match the version ranges specified in the `package.json` file.

You can update 3rd party library dependencies by running:

```
bower update
```

This will find the latest versions that match the version ranges specified in the `bower.json` file.


## Serving the Application Files

This webapp doesn't require a backend server at all, and it's recommended serving the project files 
using a local webserver during development to avoid issues with security restrictions (sandbox) in browsers. 
The sandbox implementation varies between browsers, but quite often prevents things like cookies, xhr,
etc to function properly when an html page is opened via `file://` scheme instead of `http://`.


### Running the App during Development

This project comes preconfigured with a local development webserver.
You can start this webserver with `npm start` or `gulp`

That will start server to serve static files in `dist` folder.
Also it will serve `HTTP GET` requests to the REST API in `api` folder.

Alternatively, you can choose to configure your own webserver, such as apache or nginx. Just
configure your server to serve the files under the `app/` directory.


### Running the App in Production

## Continuous Integration

### Travis CI

[Travis CI][travis] is a continuous integration service, which can monitor GitHub for new commits
to your repository and execute scripts such as building the app or running tests. The angular-seed
project contains a Travis configuration file, `.travis.yml`, which will cause Travis to run your
tests when you push to GitHub.

You will need to enable the integration between Travis and GitHub. See the Travis website for more
instruction on how to do this.

## Contact

Developers:
Aleksei Lebedev: allebdev@gmail.com

[git]: http://git-scm.com/
[bower]: http://bower.io
[npm]: https://www.npmjs.org/
[node]: http://nodejs.org
[protractor]: https://github.com/angular/protractor
[jasmine]: http://jasmine.github.io
[karma]: http://karma-runner.github.io
[travis]: https://travis-ci.org/
